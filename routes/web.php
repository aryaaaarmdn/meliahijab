<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ManajemenBarangController;
use App\Http\Controllers\TransaksiController;
use App\Models\Barang;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = Barang::all();
    return view('index', ['data' => $data]);
})->name('index');

Route::get('/dashboard', function () {
    return view('dashboard._partials.dashboard');
})->middleware(['auth'])->name('dashboard');

Route::resource('barang', ManajemenBarangController::class)
    ->middleware([
        'auth',
        'can:admin, App\Models\User'
    ]);

Route::get('/transaksi', [TransaksiController::class, 'index'])
    ->middleware(['auth'])
    ->name('transaksi');

Route::get('/transaksi/detail/{user}', [TransaksiController::class, 'detail'])
    ->middleware(['auth', 'can:admin, \App\Models\User'])
    ->name('detail');

Route::post('bayar', [TransaksiController::class, 'bayar'])
    ->middleware(['auth', 'can:user, \App\Models\User'])
    ->name('bayar');

Route::post('beli/{id}', [TransaksiController::class, 'beli'])
    ->name('beli')
    ->middleware([
        'auth',
        'can:user, App\Models\User'
    ]);

Route::post('konfirmasi', [TransaksiController::class, 'konfirmasi'])
    ->name('konfirmasi')
    ->middleware(['auth', 'can:admin, App\Models\User']);

require __DIR__.'/auth.php';
