<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/boxicons.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <title>Melia's Hijab</title>
</head>
<body>

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg py-3 sticky-top navbar-light bg-white">
        <div class="container">
            <a class="navbar-brand" href="{{ route('index') }}">
                <img class="logo" src="{{ asset('assets/img/logonavbar.png') }}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#bestseller">Best Seller</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#product">Our Hijab</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#team">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Contact</a>
                    </li>
                </ul>
                @guest
                    <a class="nav-item btn btn-dark rounded-pill tombol" href="{{ route('login') }}">
                        Login
                    </a>
                @endguest
                @auth 
                    <a class="nav-item btn btn-dark rounded-pill tombol" 
                        href="{{ route('dashboard') }}">
                        Dashboard
                    </a>
                @endauth
            </div>
        </div>
    </nav>
    <!-- //NAVBAR -->

    <!-- HERO -->
    <div class="hero vh-100 d-flex align-items-center" id="home">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mx-auto text-center">
                    <h1 class="display-4 text-white">Welcome Hijabers</h1>
                    <p class="text-white my-3">The Official Melia's Hijab Web Store</p>

                </div>
            </div>
        </div>
    </div><!-- //HERO -->

    <!-- Jumbotron -->
    <section id="bestseller">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8 mx-auto text-center">
                    <h6 class="text-primary">Best Seller</h6>
                    <h1>Explore Our Hijab</h1>
                    <p>Choose your best hijab!</p>
                </div>
            </div>

    <!-- CARD -->
            <div class="row g-4">
                <div class="col-md-4">
                    <div class="blog-post card-effect">
                        <img src="{{ asset('assets/img/best1.jpg') }}" alt="">
                        <h5 class="mt-4"><a href="#">BAWAL SHAWL BLACK</a></h5>
                        <p>RP 80.999</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="blog-post card-effect">
                        <img src="{{ asset('assets/img/best2.jpg') }}" alt="">
                        <h5 class="mt-4"><a href="#">HARAA VOAL BLACK</a></h5>
                        <p>RP 79.000</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="blog-post card-effect">
                        <img src="{{ asset('assets/img/best3.jpg') }}" alt="">
                        <h5 class="mt-4"><a href="#">MIMA SHAWL GREY</a></h5>
                        <p>RP 60.000</p>
                    </div>
                </div>
    <!-- CARD -->

    <!-- TABEL & PROGRES BAR -->
                <div class="col-md-12">
                    <div class="card">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Best Seller</th>
                                    <th scope="col">Terjual</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>BAWAL SHAWL BLACK</td>
                                    <td>1000/1000 (HABIS)
                                        <div class="progress">
                                            <div class="progress-bar bg-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                          </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>HARAA VOAL BLACK</td>
                                    <td>813/1000 
                                        <div class="progress">
                                            <div class="progress-bar bg-warning" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                          </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td>MIMA SHAWL GREY</td>
                                    <td>499/1000
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                          </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- TABEL & PROGRES BAR -->

    <!-- CARD -->
    <section id="product">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8 mx-auto text-center">
                    <h6 class="text-primary">PRODUCT</h6>
                    <h1>Explore Our Hijab</h1>
                    <p>Choose your best hijab!</p>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="margin-bottom:0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row g-3">
                @if( count($data) == 0)
                    <h3 class="text-center text-warning">Barang Kosong</h3>
                @else
                    @foreach($data as $barang)
                        <div class="col-lg-4 col-sm-6">
                            <div class="project">
                                <img src="{{ asset('upload/barang') . '/' . $barang->foto }}" 
                                    alt="$barang->nama }}">
                                <div class="overlay">
                                    <div class="">
                                        <h4 class="text-white">{{ $barang->nama }}</h4>
                                        <h6 class="text-white">Rp. {{ $barang ->harga }}</h6>
                                        <small class="text-white">Stok: {{ $barang->stok }}</small>
                                    </div>
                                    <div style="position:absolute; right:20px">
                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#{{ $barang->nama }}">
                                            Beli Sekarang
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- CARD -->

    <!-- TABEL -->
    <section class="py-0 bg-light" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-6 d-flex justify-content-center align-items-center">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/sdeqs1wuiL4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="col-md-6 py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <h6 class="text-primary">WHY TO CHOOES Melia's Hijab</h6>
                                <h1>Best Hijab For your Daily</h1>
                                <p>Kami menyediakan Hijab yang nyaman dan Stylish untuk kamu.
                                    </p>
        
                                <div class="feature d-flex mt-5">
                                    <div class="iconbox me-3">
                                        <i class='bx bxs-heart'></i>
                                    </div>
                                    <div>
                                        <h5>Full Material</h5>
                                        <p>Cotton, Diamond, Baby Doll, Ultrasatin, Voal.</p>
                                    </div>
                                </div>
                                <div class="feature d-flex">
                                    <div class="iconbox me-3">
                                        <i class='bx bxs-heart'></i>
                                    </div>
                                    <div>
                                        <h5>Best Quality</h5>
                                        <p>Kualitas Hijab sudah terbukti dengan 100+ pembeli.</p>
                                    </div>
                                </div>
                                <div class="feature d-flex">
                                    <div class="iconbox me-3">
                                        <i class='bx bxs-heart'></i>
                                    </div>
                                    <div>
                                        <h5>Trusted</h5>
                                        <p>Layanan Store terpercaya.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- TABEL -->

    <!-- TEAM -->
    <section id="team">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8 mx-auto text-center">
                    <h6 class="text-primary">TEAM</h6>
                    <h1>Melia's Team</h1>
                </div>
            </div>
            <div class="row text-center g-4">
                    <div class="team-member card-effect">
                        <img src="{{ asset('assets/img/amel.jpg') }}" alt="">
                        <h5 class="mb-0 mt-4">VIRDA AMELIA PUTRI</h5>
                        <p>Founder Melia's Hijab</p>
                        <p>Web Developer</p>
                        <div class="social-icons">
                            <a href="https://www.facebook.com/virdaameliaputribunyamin"><i class='bx bxl-facebook'></i></a>
                            <a href="https://www.instagram.com/virdaameliaaaa/"><i class='bx bxl-instagram-alt'></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- TEAM -->

    <!-- CONTACT -->
    <section id="contact">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8 mx-auto text-center">
                    <h6 class="text-primary">CONTACT</h6>
                    <h1>Get In Touch</h1>
                </div>
            </div>

            <form action="" class="row g-3 justify-content-center">
                <div class="col-md-5">
                    <input type="text" class="form-control" placeholder="Full Name">
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control" placeholder="Enter E-mail">
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="Enter Subject">
                </div>
                <div class="col-md-10">
                    <textarea name="" id="" cols="30" rows="5" class="form-control"
                        placeholder="Enter Message"></textarea>
                </div>
    <!-- MODAL -->
                <div class="col-md-10 d-grid">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">Send</button>
                    <div class="modal" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Keterangan</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="mb-3">
                                            <label class="form-label required">Pesan Telah Terkirim</label>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
            <div class="container mt-5">
    <!-- MODAL -->

    <!-- MAPS -->
            <div class="col-md-12 pt-5">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.9007141960683!2d107.61662131414452!3d-6.902475869466878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e64c5e8866e5%3A0x37be7ac9d575f7ed!2sGedung%20Sate!5e0!3m2!1sen!2sid!4v1634987060499!5m2!1sen!2sid" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </section>
    <!-- MAPS -->

    <!-- FOOTER -->
    <footer>
        <div class="footer-bottom py-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p class="mb-0">made by Virda Amelia Putri | Designed with <i
                            class="bx bx-heart text-danger"></i> by<a
                                href="https://www.instagram.com/virdaameliaaaa/"
                                class="text-white">AMEL</a></p>
                    </div>
                    <div class="col-md-6">
                        <div class="social-icons">
                            <a href="https://www.facebook.com/virdaameliaputribunyamin""><i class='bx bxl-facebook'></i></a>
                            <a href="https://www.instagram.com/virdaameliaaaa/"><i class='bx bxl-instagram-alt'></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER -->

    <!-- Modal -->
    <div id="modal-wrapper">
        @foreach($data as $barang)
            <form action="{{ route('beli', $barang->id) }}" method="post">
                @csrf
                <div class="modal fade modal-transaksi" id="{{ $barang->nama }}" 
                tabindex="-1" aria-labelledby="exampleModalLabel" 
                    aria-hidden="true" data-harga="{{ $barang->harga }}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-light">
                                <h5 class="modal-title" id="exampleModalLabel">{{ $barang->nama }}</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="mb-3">
                                    <label for="stok">Stok</label>
                                    <input type="text" class="form-control stok"
                                        value="{{ $barang->stok }}" readonly
                                        name="stok">
                                </div>
                                <div class="mb-3">
                                    <label for="jumlah">Jumlah</label>
                                    <input type="text" class="jumlah" 
                                        class="form-control" name="jumlah" autofocus>
                                </div>
                                <div class="mb-3">
                                    <label for="total">Total Harga</label>
                                    <input type="text" id="total" readonly disabled
                                        class="form-control total" name="total">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Beli</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @endforeach
    </div>

    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/wow.js') }}"></script>
    <script>
        new WOW().init();
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    @if( session('sukses') )
        @php
            if( session('sukses') ) {
                echo '<script>
                    alert("'. session('sukses') .'");
                </script>';
            }
        @endphp
    @endif

    if (  )

</body>
</html>