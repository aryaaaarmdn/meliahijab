@extends('dashboard.index')
@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Manajemen Barang</h1>
    <p class="mb-4">
        Silahkan acak acak, obrak abrik sesuka hati anda
    </p>

    @if( session('status') )
        <div class="alert alert-{{ session('alert') }}">
            {{ session('status') }}
        </div>
    @endif

    <!-- Table -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">List Barang</h6>
        </div>
        <div class="card-body">
            <a href="{{ route('barang.create') }}" class="btn btn-info btn-icon-split"
                style="margin-bottom:.5em;">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Tambah Data</span>
            </a>
            <div class="table-responsive">
                <table class="table table-bordered" 
                    id="dataTable" width="100%" 
                    cellspacing="0">
                    <thead>
                        <tr class="text-center">
                            <th>Nama</th>
                            <th>Gambar</th>
                            <th>Harga</th>
                            <th>Stok</th>
                            <th>Modifikasi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $barang)
                            <tr>
                                <td class="text-center">{{ $barang->nama }}</td>
                                <td class="d-flex justify-content-center">
                                    <img width="200" 
                                        src="{{ asset('upload/barang') . '/' . $barang->foto }}" alt="Barang image">
                                </td>
                                <td class="text-center">{{ $barang->harga }}</td>
                                <td class="text-center">{{ $barang->stok }}</td>
                                <td>
                                    <div class="d-flex justify-content-center align-items-center">
                                        <div>
                                            <a class="btn btn-flat btn-primary" 
                                                href="{{ route('barang.edit', $barang->id) }}">
                                                Edit
                                            </a>
                                        </div>
                                        <div>
                                            <form action="{{ route('barang.destroy', $barang->id) }}"
                                                method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-flat btn-danger"
                                                    type="submit"
                                                    onclick="return confirm('yakin?');">
                                                    Delete
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
