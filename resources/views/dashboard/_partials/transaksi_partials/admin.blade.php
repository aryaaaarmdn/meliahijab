@extends('dashboard.index')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Transaksi</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" 
                    id="dataTable" width="100%" 
                    cellspacing="0">
                    <thead>
                        <tr class="text-center">
                            <th>Nama Pembeli</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data_pembeli as $data)
                            <tr class="text-center">
                                <td>{{ $data->name }}</td>
                                <td>
                                    <a href="{{ route('detail', $data->id) }}" 
                                        class="badge badge-info">Detail</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection