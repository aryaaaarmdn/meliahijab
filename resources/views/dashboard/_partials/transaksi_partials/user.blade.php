@extends('dashboard.index')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Transaksi</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" 
                    id="dataTable" width="100%" 
                    cellspacing="0">
                    @if(session('sukses'))
                        <div class="alert alert-success">
                            {{ session('sukses') }}
                        </div>
                    @endif
                    <div class="d-flex flex-row w-50 justify-content-around align-items-center mb-3">
                        <h4 style="margin-bottom:0">Total Tagihan: </h4>
                        @if($total_tagihan[0] != 0) 
                            <h4 style="margin-bottom:0">Rp. {{ $total_tagihan[0] }}</h4>
                            <form action="{{ route('bayar') }}" method="post">
                                @csrf
                                @foreach($total_tagihan[1] as $data)
                                    <input type="hidden" name="produk_id[]" value="{{ $data }}">
                                @endforeach
                                @foreach($total_tagihan[2] as $data)
                                    <input type="hidden" name="transaksi_id[]" value="{{ $data }}">
                                @endforeach
                                <button type="submit" class="btn btn-primary">Bayar</button>
                            </form>
                        @else
                            <h4 style="margin-bottom:0">-</h4>
                        @endif
                    </div>
                    <thead>
                        <tr class="text-center">
                            <th>Produk</th>
                            <th>Harga</th>
                            <th>Jumlah</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transaksi as $barang)
                            <tr class="text-center">
                                <td>{{ $barang->nama }}</td>
                                <td>{{ $barang->harga }}</td>
                                <td>{{ $barang->pivot->jumlah }}</td>
                                <td>{{ $barang->pivot->total_harga() }}</td>
                                <td>{{ $barang->pivot->status() }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection