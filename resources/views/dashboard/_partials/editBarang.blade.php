@extends('dashboard.index')
@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Manajemen Barang</h1>
    <p class="mb-4">
        Silahkan acak acak, obrak abrik sesuka hati anda
    </p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tambah Barang</h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="margin-bottom:0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('barang.update', $data->id) }}" 
                method="post" 
                enctype="multipart/form-data"
            >
                @method('PUT')
                @csrf
                <div class="form-group">
                    <input type="text" class="form-control"
                        name="nama" id="nama" value="{{ old('nama', $data->nama) }}"
                        placeholder="Masukkan Nama Produk">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="harga"
                        name="harga" placeholder="Masukkan Nominal Harga"
                        value="{{ old('harga', $data->harga) }}">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="stok"
                        name="stok" placeholder="Masukkan Jumlah Stok"
                        value="{{ old('stok', $data->stok) }}">
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="gambar"
                        id="customFile" name="gambar">
                    <label class="custom-file-label" for="customFile">
                        {{ $data->foto }}
                    </label>
                </div>
                <div class="mt-4">
                    <button class="btn btn-primary addProduk" type="submit">
                        Edit
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection