@extends('dashboard.index')
@section('content')
    @can('admin', App\Models\User::class)
        @include('dashboard._partials.transaksi_partials.admin')
    @elsecan('user', App\Models\User::class)
        @include('dashboard._partials.transaksi_partials.user')
    @endcan
@endsection