<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="{{ asset('assets/css/login.css') }}">
</head>

<body>
    <div class="kotak_login">
        <h2 class="tulisan_login">Login</h2>
        @if (session()->has('error'))
            <div class="alert alert-danger">
                <small style="color:red ">{{ session('error') }}</small>
            </div>
        @endif

        <form action="{{ route('login') }}" method="POST">
            @csrf
            <label>Username</label>
            <input type="text" name="email" class="form_login" placeholder="email">

            <label>Password</label>
            <input type="password" name="password" class="form_login" placeholder="Password ">

            <input type="submit" class="tombol_login" value="LOGIN">

            <br />
            <br />
            <div class="d-flex justify-content-around">
                <div>
                    <small>
                        Belum Punya akun ? Daftar 
                        <a href="{{ route('register') }}">Disini</a>
                    </small>
                </div>
                <div>
                    <small>
                        <a class="link" href="{{ route('index') }}">Back</a>
                    </small>
                </div>
            </div>
        </form>

    </div>
</body>

</html>