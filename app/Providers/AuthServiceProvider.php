<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\Response;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Gate::define('user', function (User $user) {
            return $user->is_admin === 0 ? 
                Response::allow() : Response::deny('Hayoo, Admin Tidak Bisa Melakukan Ini wkek');
        });

        Gate::define('admin', function (User $user) {
            return $user->is_admin === 1 ? 
                Response::allow() : Response::deny('Hayoo,User Biasa Tidak Bisa Melakukan Ini wkek');
        });
    }
}
