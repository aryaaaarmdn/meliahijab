<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ManajemenBarangController extends Controller
{
    /** 
     * Validasi Input ( Refactor/Mempersingkat Logic )
     * Agar minimalisir duplikasi code yang sama
     * Karena validasi ketika tambah data barang & edit barang, logicnya hampir mirip
     * hanya beda di field foto/gambar
    */
    public function validasi(Request $data)
    {
        if ( $data->method() == 'POST' ) {

            $data->validate([
                'nama' => 'required',
                'harga' => 'required',
                'stok' => 'required',
                'gambar' => 'required|image|mimes:jpg,png,jpeg|max:2048',
            ]);

        } elseif ( $data->method() == 'PUT' ) {
            
            $data->validate([
                'nama' => 'required',
                'harga' => 'required',
                'stok' => 'required',
                'gambar' => 'nullable',
            ]);

        }
    }

    /**
     * Input Value ke Model
     * Karena logic pada add & edit data ketika input data yaitu sama
     */
    public function inputData(Barang $model, Request $request)
    {
        $model->nama = $request->nama;
        $model->harga = $request->harga;
        $model->stok = $request->stok;

        return $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::all();
        return view('dashboard._partials.manajemen_barang', ['data' => $barang]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard._partials.tambahBarang');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validasi($request);

        // Instansiasi model dan masukkan data input kedalam model
        $barang = new Barang;
        $this->inputData($barang, $request);
        
        $barang->foto = $request->file('gambar')->hashName();
        $request->file('gambar')
                ->move(public_path('upload/barang/'), $request->file('gambar')->hashName());

        // Simpan mode ke db
        $save = $barang->save();

        // Interaksi respon 
        if( $save ) {
            return redirect()->route('barang.index')
                    ->with('status', 'Berhasil Menambahkan Barang')
                    ->with('alert', 'success');
        } else {
            return redirect()->route('barang.index')
                    ->with('status', 'Gagal Menambahkan Barang')
                    ->with('alert', 'danger');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Barang $barang)
    {
        return view('dashboard._partials.editBarang', ['data' => $barang]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barang $barang)
    {
        /* 
            Note selagi muncul dipikiran, keburu lupa haha..
            Untuk restore value/text file (foto) kedalam input type, hanya merestore nama file saja,
            karena file itu sendiri hanya ada didalam storage, bukan database, yang didatabase hanya nama file saja.
            Jadi, ketika file lama muncul, itu sama dengan kosong karena nama file tidak sama 
            dengan file nyaa
        */

        $this->validasi($request);
        $this->inputData($barang, $request);
        
        if ( $request->hasFile('gambar') ) {
            unlink(public_path('upload/barang/') . $barang->foto);
            $barang->foto = $request->file('gambar')->hashName();
            $request->file('gambar')
                    ->move(public_path('upload/barang/'), $request->file('gambar')->hashName());
        }

        $save = $barang->save();

        if( $save ) {
            return redirect()->route('barang.index')
                ->with('status', 'Berhasil Edit Barang')
                ->with('alert', 'success');
        } else {
            return redirect()->route('barang.index')
                ->with('status', 'Gagal Edit Barang')
                ->with('alert', 'danger');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Barang $barang)
    {
        unlink(public_path('upload/barang/') . $barang->foto);
        if ( $barang->delete() ) {
            return redirect()->route('barang.index')
                    ->with('status', 'Berhasil Hapus Barang')
                    ->with('alert', 'success');
        } else {
            return redirect()->route('barang.index')
                ->with('status', 'Gagal Hapus Barang')
                ->with('alert', 'danger');
        }

    }
}
