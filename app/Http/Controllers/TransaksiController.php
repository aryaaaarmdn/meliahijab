<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

use App\Models\Barang;
use App\Models\BarangUser;
use App\Models\User;

class TransaksiController extends Controller
{

    public function total_tagihan($sumber)
    {
        $hasil = 0;
        $barang = [];
        $transaksi = [];

        foreach ($sumber->pending as $data) {
            $hasil += ( $data->harga * $data->pivot->jumlah );
            $barang[] = $data->id;
            $transaksi[] = $data->pivot->id;
        }

        return [$hasil, $barang, $transaksi];
    }

    public function index()
    {
        // Get Data transaksi (Untuk data pada halaman user/buyer)
        $total_tagihan_user = $this->total_tagihan(auth()->user());
        $barang = auth()->user()->barang;

        // Get Data transaksi (Untuk data pada halaman admin)
        // Get data user yang telah transaksi
        $user = User::has('barang')->get();
        
        return view('dashboard._partials.transaksi', [
            'transaksi' => $barang,
            'total_tagihan' => $total_tagihan_user,
            'data_pembeli' => $user,
        ]);
    }

    public function detail(User $user)
    {
        // foreach($user->waiting as $data) {
        //     dump($data->pivot->id);
        // }
        // die;
        return view('dashboard._partials.transaksi_partials.detail', [
            'transaksi' => $user->barang,
            'waiting' => $user->waiting,
            'user' => $user,
        ]);
    }


    public function beli(Request $request, Barang $id)
    {
        // dd($id);
        $request->validate([
            'jumlah' => 'required|numeric|lte:'. $request->stok,
        ]);

        $user = auth()->user();
        $user->barang()->attach($id, ['jumlah' => $request->jumlah]);

        return redirect()->back()
                ->with('sukses', 'Berhasil menambahkan barang, cek dashboard untuk cek');
    }

    public function bayar(Request $request)
    {
        foreach($request->transaksi_id as $data) {
            $barangUser = BarangUser::findOrFail($data);
            $barangUser->is_paid = 1;
            $barangUser->save();
        }
        
        return redirect()->back()
                ->with('sukses', 'Berhasil bayar, harap tunggu konfirmasi');
    }

    public function konfirmasi(Request $request)
    {
        foreach($request->transaksi_id as $data) {
            $barangUser = BarangUser::findOrFail($data);
            $barangUser->is_paid = 2;
            $barangUser->save();
        }
        
        return redirect()->back()
                ->with('sukses', 'Berhasil Konfirmasi, Transaksi Selesai');
    }

}
