<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $timestamps = true;

     // Relasi Many to Many (Barang - User / Transaksi)
    public function barang()
    {
        return $this->belongsToMany(Barang::class)
                ->using(BarangUser::class)
                ->withPivot('jumlah', 'is_paid')
                ->withTimestamps();
    }

    // Relasi Many to Many (Barang - User / Transaksi, Khusus status pending)
    public function pending()
    {
        return $this->belongsToMany(Barang::class)
                ->using(BarangUser::class)
                ->withPivot('jumlah', 'is_paid', 'id')
                ->wherePivot('is_paid', 0);
    }

    public function waiting()
    {
        return $this->belongsToMany(Barang::class)
                ->using(BarangUser::class)
                ->withPivot('jumlah', 'is_paid', 'id')
                ->wherePivot('is_paid', 1);
    }

}
