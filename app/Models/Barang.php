<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;
    protected $table = 'barang';
    public $timestamps = true;

    // Relasi Many to Many (Barang - User / Transaksi)
    public function user()
    {
        return $this->belongsToMany(User::class)
                ->using(BarangUser::class)
                ->withTimestamps();
    }
}

