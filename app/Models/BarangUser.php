<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class BarangUser extends Pivot
{
    protected $table = 'barang_user';
    // protected $fillable = ['jumlah', 'is_paid'];
    public $incrementing = true;
    public $timestamps = true;


    public function status()
    {
        if ($this->is_paid == 0) {
            return 'Pending';
        } else if ( $this->is_paid == 1) {
            return 'Waiting';
        } else {
            return 'Success';
        }
    }

    public function barang()
    {
        return $this->belongsTo(Barang::class);
    }

    public function total_harga()
    {
        return $this->jumlah * $this->barang->harga;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
