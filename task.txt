- Membuat Database dari aplikasi quiz kemarin
- Memakai methode GET & POST dari menu registrasi dan login
- Membuat CRUD dari aplikasi minggu ke 6-7 ( dari admin muncul di user )
- Membuat Session dan cookies minggu 8-9
- membuat transaksi dari web antar 2 role (admin & user)

Progress:
- 14 Desember   : 
    (+) Instalasi Laravel
    (+) konfigurasi (DB, Laravel Breeze/Autentikasi, Migration Table user)
    (+) Templatting / Bedah Tampilan

- 15 Desember   :
    (+) Update Logic Auth/login (pada bagian is_admin)
    (+) Add Middleware ( admin & user )
    (+) Tambah Halaman Admin & User Dashboard (1/2)

- 16 Desember   :
    (+) Tambah Halaman Admin & User Dashboard (2/2)
        - Berdasarkan otorisasi
    (+) Membuat Otorisasi antara user dan admin (Laravel Gate)
    (+) Membuat table table yang berhubungan dengan transaksi serta modelnya
    (+) Membuat relasi agar user bisa bertransaksi dengan admin
        Relasi: 
            Many to Many (model user & model barang)
            nama Table Pivot = barang_user
    (+) Membuat ManajemenBarangController --resource --model=NamaModelDariTabelBarang
    (+) View menu manajemen barang
    
- 19 Desember  :
    (+) CRUD manajemen barang
    (+) Membuat Otorisasi untuk CRUD Barang via route middleware
        CRUD barang hanya bisa dilakukan oleh admin, user biasa tidak bisa menambahkan barang baru.
        Maka dari itu, dibuat otorisasi agar user tidak bisa CRUD barang
    (+) Membuat otorisasi agar admin tidak bisa membeli barang

- 21 Desember   :
    (+) Kalkulasi via js (event listener) agar ketika user menginput data jumlah barang 
        yang ingin dibeli, maka secara realtime field total harga juga akan berubah.
        dan nantinya data ini akan dimasukkan ke db (done)

- 23 - 24 Desember  :
    (+) Proses transaksi
        - Pembayaran disimulasikan dengan input
        - Alur:
            1. User memesan lalu status pending karena belum bayar (Checkout)
            2. user bayar, maka status berubah menjadi menunggu 
                Note: Total pembayaran mengambil dari keseluruhan keranjang (tidak satusatu)
            3. Admin acc dan status transaksi pada user berubah menjadi berhasil
        - Status:
            1. Pending / Belum Bayar
            2. Waiting / Sudah Bayar tetapi belum di acc oleh admin
            3. Sukses / Sudah di acc oleh admin, transaksi selesai
    (+) Sepertinya sedikit modifikasi pada table barang_user  
        - Menambah Kolom is_paid

- Last Step Maybe :
    (+) Mendinamiskan stok ketika bertransaksi, agar tidak hanya sebagai formalitas..
    (+) Fitur registrasi user (Done)
    (+) Testing
    (+) Merapihkan UI 