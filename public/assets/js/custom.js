const modal = document.querySelectorAll('.modal-transaksi');

const ModalWrapper = document.getElementById('modal-wrapper')

ModalWrapper.addEventListener('show.bs.modal', (e) => {
    const Harga = parseInt(e.target.dataset.harga);
    const JumlahElement = e.target.querySelector(
        '.modal-dialog .modal-content .modal-body input.jumlah'
    )
    const TotalHargaElement = e.target.querySelector(
        '.modal-dialog .modal-content .modal-body .total'
    )
    const StokElement = e.target.querySelector(
        '.modal-dialog .modal-content .modal-body .stok'
    )

    JumlahElement.addEventListener('input', (event) => {

        if ( event.target.value == '' ) {
            TotalHargaElement.value = null
        } else {
            // Jika Input non angka
            if ( !/^\d+$/.test(event.target.value) ) {
                alert('Harus angka')
                TotalHargaElement.value = null
            } else { // Jika input adalah angka
                if ( parseInt(event.target.value) > parseInt(StokElement.value) ) {
                    alert('Stok barang tidak mencukupi')
                    TotalHargaElement.value = null
                } else {
                    TotalHargaElement.value = parseInt(event.target.value) * Harga
                }
            }
        }
             
    })

})

// Array.from(modal).forEach(modal => {
    // modal.addEventListener('shown.bs.modal', (e) => {
    //     const Harga = e.target.dataset.harga
    //     const JumlahElement = e.target.querySelector(
    //             '.modal-dialog .modal-content .modal-body input.jumlah'
    //         )
    //     const TotalHargaElement = e.target.querySelector(
    //         '.modal-dialog .modal-content .modal-body .total'
    //     )
    //     const StokElement = e.target.querySelector(
    //         '.modal-dialog .modal-content .modal-body .stok'
    //     )
    //     JumlahElement.addEventListener('input', (event) => {

    //         if ( event.target.value == '' ) {
    //             TotalHargaElement.value = null
    //         } else {
    //             if ( event.target.value > StokElement.value  ) {
    //                 alert('Stok barang tidak mencukupi')
    //                 TotalHargaElement.value = null
    //             } else if ( !/^\d+$/.test(event.target.value) ) {
    //                 Alert('Harus angka')
    //                 TotalHargaElement.value = null
    //             } else {
    //                 TotalHargaElement.value = event.target.value * Harga
    //             }
    //         }
               
    //     })
    // })
// })